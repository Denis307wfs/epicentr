package com.stepanov.dao;

import com.stepanov.dto.ProductDTO;
import com.stepanov.util.ConnectionManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductDAOTest {
    private ProductDAO tested;
    @Mock
    private ConnectionManager connectionManagerMock;
    @Mock
    private Connection connectionMock;
    @Mock
    private PreparedStatement statementMock;

    @BeforeEach
    void setUp() throws SQLException {
        tested = new ProductDAO(connectionManagerMock);

        try (MockedStatic<ConnectionManager> mockedStatic = mockStatic(ConnectionManager.class)) {
            mockedStatic.when(ConnectionManager::getInstance).thenReturn(connectionManagerMock);
        }
        when(connectionManagerMock.getConnection()).thenReturn(connectionMock);
        doNothing().when(connectionMock).commit();
        when(connectionMock.prepareStatement(anyString())).thenReturn(statementMock);
        doNothing().when(statementMock).setString(anyInt(), anyString());
        doNothing().when(statementMock).setInt(anyInt(), anyInt());
    }

    @Test
    void shouldCallInsertMethod() throws SQLException {
        List<ProductDTO> list = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            list.add(new ProductDTO("product" + i, i));
        }

        tested.insert(list);

        verify(connectionMock, times(1)).prepareStatement(anyString());
        verify(statementMock, times(3)).setString(anyInt(), anyString());
        verify(statementMock, times(3)).setInt(anyInt(), anyInt());
        verify(statementMock, times(3)).addBatch();
        verify(statementMock, times(1)).executeBatch();
    }

    @Test
    void shouldCallInsertMethodWithFullBatch() throws SQLException {
        List<ProductDTO> list = new ArrayList<>();
        for (int i = 1; i <= 1000; i++) {
            list.add(new ProductDTO("product" + i, i));
        }

        tested.insert(list);

        verify(connectionMock, times(1)).prepareStatement(anyString());
        verify(statementMock, times(1000)).setString(anyInt(), anyString());
        verify(statementMock, times(1000)).setInt(anyInt(), anyInt());
        verify(statementMock, times(1000)).addBatch();
        verify(statementMock, times(2)).executeBatch();
    }
}