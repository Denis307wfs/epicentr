package com.stepanov.dao;

import com.stepanov.util.ConnectionManager;
import com.stepanov.util.PropertiesUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StoreDAOTest {
    private StoreDAO tested;
    @Mock
    private ConnectionManager connectionManagerMock;
    @Mock
    private Connection connectionMock;
    @Mock
    private PreparedStatement statementMock;
    @Mock
    private ResultSet rsMock;

    @BeforeEach
    void setUp() throws SQLException {
        PropertiesUtil.PROPERTIES.setProperty("numberOfProducts", "100");
        tested = new StoreDAO(connectionManagerMock);

        try (MockedStatic<ConnectionManager> mockedStatic = mockStatic(ConnectionManager.class)) {
            mockedStatic.when(ConnectionManager::getInstance).thenReturn(connectionManagerMock);
        }

        when(connectionManagerMock.getConnection()).thenReturn(connectionMock);
        when(connectionMock.prepareStatement(anyString())).thenReturn(statementMock);

    }

    @Test
    void shouldCallInsertMethod() throws SQLException {
        tested.insert();

        verify(connectionMock, times(1)).prepareStatement(anyString());
        verify(statementMock, times(6000)).setInt(anyInt(), anyInt());
        verify(statementMock, times(2000)).addBatch();
        verify(statementMock, times(3)).executeBatch();
    }

    @Test
    void shouldCallFindMaxCountOfProductsInShopByTypeMethod() throws SQLException {
        when(statementMock.executeQuery()).thenReturn(rsMock);

        tested.findMaxCountOfProductsInShopByType("Меблі");

        verify(statementMock, times(1)).setString(anyInt(), anyString());
        verify(statementMock, times(1)).executeQuery();
    }
}