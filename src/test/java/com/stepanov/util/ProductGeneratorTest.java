package com.stepanov.util;

import com.stepanov.dto.ProductDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


class ProductGeneratorTest {
    private ProductGenerator tested;

    @Test
    void nameLengthTest() {
        PropertiesUtil.PROPERTIES.setProperty("numberOfProducts", "100");
        tested = new ProductGenerator(new MyValidator());
        List<ProductDTO> products = tested.generateProducts();

        int maxNameLength = -100;
        int minNameLength = 100;
        int nameLength;
        for (ProductDTO product : products) {
            nameLength = product.getName().length();
            maxNameLength = Math.max(maxNameLength, nameLength);
            minNameLength = Math.min(minNameLength, nameLength);
        }

        Assertions.assertTrue(maxNameLength <= 12);
        Assertions.assertTrue(minNameLength >= 3);
        Assertions.assertEquals(100, products.size());
    }

    @Test
    void typeIdTest() {
        PropertiesUtil.PROPERTIES.setProperty("numberOfProducts", "100");
        tested = new ProductGenerator(new MyValidator());
        List<ProductDTO> products = tested.generateProducts();

        int maxTypeId = -100;
        int minTypeId = 100;
        for (ProductDTO product : products) {
            maxTypeId = Math.max(maxTypeId, product.getTypeId());
            minTypeId = Math.min(minTypeId, product.getTypeId());
        }

        Assertions.assertTrue(maxTypeId <= 20);
        Assertions.assertTrue(minTypeId >= 1);
        Assertions.assertEquals(100, products.size());
    }
}