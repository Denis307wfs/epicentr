package com.stepanov;

import com.stepanov.util.PropertiesUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PropertiesUtilTest {

    @Test
    void correctPropertyTest() {
        Assertions.assertDoesNotThrow(() -> PropertiesUtil.PROPERTIES);
    }

    @Test
    void loadInternalPropertyTest() {
        String username = PropertiesUtil.get("username");
        Assertions.assertEquals("postgres", username);
    }
}