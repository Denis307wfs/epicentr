DROP SCHEMA IF EXISTS epicentr CASCADE;

CREATE SCHEMA epicentr;


CREATE TABLE epicentr.shop
(
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(30),
    address VARCHAR(100) NOT NULL
);


CREATE TABLE epicentr.type
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL
);


CREATE TABLE epicentr.product
(
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(30) NOT NULL,
    type_id INT         NOT NULL,
    FOREIGN KEY (type_id) REFERENCES epicentr.type (id)
);



CREATE TABLE epicentr.store
(
    id         SERIAL PRIMARY KEY,
    shop_id    INT NOT NULL,
    product_id INT NOT NULL,
    amount     INT NOT NULL,
    FOREIGN KEY (shop_id) REFERENCES epicentr.shop (id),
    FOREIGN KEY (product_id) REFERENCES epicentr.product (id)
);