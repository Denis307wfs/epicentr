package com.stepanov.dao;

import com.stepanov.util.ConnectionManager;

public class DAOFactory {

    public static ProductDAO getProductDAO() {
        return new ProductDAO(ConnectionManager.getInstance());
    }

    public static StoreDAO getStoreDAO() {
        return new StoreDAO(ConnectionManager.getInstance());
    }
}
