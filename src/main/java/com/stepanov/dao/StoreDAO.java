package com.stepanov.dao;

import com.stepanov.util.ConnectionManager;
import com.stepanov.util.PropertiesUtil;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

public class StoreDAO {
    private final Logger log = LoggerFactory.getLogger(StoreDAO.class);
    private final Random random;
    private final ConnectionManager connectionManager;
    private static final int NUMBER_OF_SHOPS = Integer.parseInt(PropertiesUtil.get("numberOfShops"));
    private static final int NUMBER_OF_PRODUCTS = Integer.parseInt(PropertiesUtil.get("numberOfProducts"));
    private static final int MAX_AMOUNT_OF_PRODUCTS = Integer.parseInt(PropertiesUtil.get("maxAmountOfProducts"));
    private static final String INSERT_SQL = "INSERT INTO epicentr.store (shop_id, product_id, amount) VALUES (?, ?, ?)";
    private static final String FIND_MAX_SQL = "" +
            "SELECT sh.address AS address, SUM(st.amount) AS sum_amount " +
            "FROM epicentr.store AS st " +
            "         JOIN epicentr.shop AS sh ON sh.id = st.shop_id " +
            "         JOIN epicentr.product AS p ON p.id = st.product_id " +
            "         JOIN epicentr.type AS t on t.id = p.type_id " +
            "WHERE t.name = ? " +
            "GROUP BY sh.address, t.name " +
            "ORDER BY sum_amount DESC " +
            "FETCH FIRST 1 ROW ONLY;";

    public StoreDAO(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        this.random = new Random();
    }

    public void insert() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        int batchSize = 1000;
        int currentBatchCounter = 0;
        int numOfBatch = 0;

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_SQL)) {
            connection.setAutoCommit(false);

            for (int i = 1; i <= NUMBER_OF_SHOPS; i++) {
                for (int j = 1; j <= NUMBER_OF_PRODUCTS; j++) {
                    statement.setInt(1, i);
                    statement.setInt(2, j);
                    statement.setInt(3, random.nextInt(MAX_AMOUNT_OF_PRODUCTS) + 1);

                    statement.addBatch();
                    currentBatchCounter++;

                    if (currentBatchCounter == batchSize) {
                        statement.executeBatch();
                        connection.commit();
                        log.debug("BATCH {} with {} lines inserted: {} ms",
                                ++numOfBatch, currentBatchCounter, stopWatch.getTime());
                        currentBatchCounter = 0;
                    }
                }
            }

            statement.executeBatch();
            connection.commit();
            log.debug("BATCH {} with {} lines inserted", ++numOfBatch, currentBatchCounter);

            connection.setAutoCommit(true);
        } catch (SQLException e) {
            log.error("Error inserting in store", e);
        }
        stopWatch.stop();
        int totalNumbersOfLines = NUMBER_OF_SHOPS * NUMBER_OF_PRODUCTS;
        double insertDuration = (double) stopWatch.getTime() / 1000;
        log.info("Inserted {} lines for {} seconds", totalNumbersOfLines, insertDuration);
        log.info("RPS: {}", totalNumbersOfLines / insertDuration);
    }

    public String findMaxCountOfProductsInShopByType(String type) {
        String address = null;

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_MAX_SQL)) {
            statement.setString(1, type);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                address = rs.getString("address");
            }
        } catch (SQLException e) {
            log.error("Error finding max count in shop by type = {}", type, e);
        }
        stopWatch.stop();
        double selectDuration = (double) stopWatch.getTime() / 1000;
        log.info("Shop address with max count of products by {} type found for {} seconds", type, selectDuration);

        return address;
    }
}
