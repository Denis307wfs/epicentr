package com.stepanov.dao;

import com.stepanov.dto.ProductDTO;
import com.stepanov.util.ConnectionManager;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ProductDAO {
    private final Logger log = LoggerFactory.getLogger(ProductDAO.class);
    private final ConnectionManager connectionManager;
    private static final String INSERT_SQL = "INSERT INTO epicentr.product (name, type_id) VALUES (?, ?)";

    public ProductDAO(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void insert(List<ProductDTO> products) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        int batchSize = 1000;
        int currentBatchCounter = 0;
        int numOfBatch = 0;

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(INSERT_SQL)) {
            connection.setAutoCommit(false);

            for (ProductDTO product : products) {
                statement.setString(1, product.getName());
                statement.setInt(2, product.getTypeId());

                statement.addBatch();
                currentBatchCounter++;

                if (currentBatchCounter == batchSize) {
                    statement.executeBatch();
                    connection.commit();
                    log.debug("BATCH {} with {} lines inserted: {} ms",
                            ++numOfBatch, currentBatchCounter, stopWatch.getTime());
                    currentBatchCounter = 0;
                }
            }

            statement.executeBatch();
            connection.commit();
            log.debug("BATCH {} with {} lines inserted", ++numOfBatch, currentBatchCounter);

            connection.setAutoCommit(true);
        } catch (SQLException e) {
            log.error("Error inserting products", e);
        }
        stopWatch.stop();
        double insertDuration = (double) stopWatch.getTime() / 1000;
        log.info("Inserted {} products for {} seconds", products.size(), insertDuration);
        log.info("RPS: {}", products.size() / insertDuration);
    }
}
