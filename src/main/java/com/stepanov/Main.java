package com.stepanov;

import com.stepanov.dto.ProductDTO;
import com.stepanov.service.ProductService;
import com.stepanov.service.ProductServiceImpl;
import com.stepanov.service.StoreService;
import com.stepanov.service.StoreServiceImpl;
import com.stepanov.util.ConnectionManager;
import com.stepanov.util.MyValidator;
import com.stepanov.util.ProductGenerator;
import com.stepanov.util.SQLExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOG.debug("-------START PROGRAM----------");

        String type = System.getProperty("type");
        if (type == null || type.isEmpty()) {
            type = "Меблі";
            LOG.info("System param is empty. So type = \"Меблі\"");
        }

        ConnectionManager connectionManager = ConnectionManager.getInstance();
        SQLExecutor sqlExecutor = new SQLExecutor(connectionManager);

        // create tables
        sqlExecutor.executeFile("src/main/resources/sql/create_tables.sql");
        // insert types and shops in tables
        sqlExecutor.executeFile("src/main/resources/sql/insert_data.sql");

        MyValidator validator = new MyValidator();
        ProductGenerator productGenerator = new ProductGenerator(validator);
        List<ProductDTO> products = productGenerator.generateProducts();

        ProductService productService = new ProductServiceImpl();
        productService.insertProducts(products);

        StoreService storeService = new StoreServiceImpl();
        storeService.insertStoreProducts();
        String address = storeService.getShopAddressWithMaxCountOfProductsByType(type);

        LOG.info("Shop address: {}", address);

        LOG.debug("-------FINISH PROGRAM----------");
    }
}
