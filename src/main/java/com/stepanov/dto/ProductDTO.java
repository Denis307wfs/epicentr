package com.stepanov.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

public class ProductDTO {
    private int id;
    @NotNull (message = "Name has not to be null")
    @Length(min = 3, max = 12, message = "Name length out of bounds [3; 12]")
    private String name;
    @Min(value = 1, message = "Type id must be more than 0")
    @Max(value = 20, message = "Type id must not exceed 20")
    private int typeId;

    public ProductDTO(String name, int typeId) {
        this.name = name;
        this.typeId = typeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", typeId=" + typeId +
                '}';
    }
}
