package com.stepanov.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public final class ConnectionManager {
    public static final String DB_DRIVER = "db.driver";
    public static final String DB_URL = "db.url";
    public static final String USER_NAME = "username";
    public static final String PASSWORD = "password";
    private static volatile ConnectionManager instance;
    private final HikariDataSource dataSource;

    private ConnectionManager() {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(PropertiesUtil.get(DB_DRIVER));
        config.setJdbcUrl(PropertiesUtil.get(DB_URL));
        config.setUsername(PropertiesUtil.get(USER_NAME));
        config.setPassword(PropertiesUtil.get(PASSWORD));
        config.setMaximumPoolSize(3);
        config.setIdleTimeout(1000 * 60 * 60 * 24);
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", 250);
        config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);

        dataSource = new HikariDataSource(config);
    }

    public static ConnectionManager getInstance() {
        ConnectionManager localInstance = instance;
        if (localInstance == null) {
            synchronized (ConnectionManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new ConnectionManager();
                }
            }
        }
        return localInstance;
    }

    public Connection getConnection() {
        Connection connection;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            throw new IllegalStateException("Cannot obtain a connection", e);
        }
        return connection;
    }

}
