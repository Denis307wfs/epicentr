package com.stepanov.util;

import com.stepanov.dto.ProductDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Stream;

public class ProductGenerator {
    private final Logger log = LoggerFactory.getLogger(ProductGenerator.class);
    private final Random random;
    private final MyValidator validator;
    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 15;
    private static final int NUMBER_OF_TYPES = Integer.parseInt(PropertiesUtil.get("numberOfTypes"));
    private static final int NUMBER_OF_PRODUCTS = Integer.parseInt(PropertiesUtil.get("numberOfProducts"));

    public ProductGenerator(MyValidator validator) {
        random = new Random();
        this.validator = validator;
    }

    public List<ProductDTO> generateProducts() {
        List<ProductDTO> products = new ArrayList<>(NUMBER_OF_PRODUCTS);
        log.info("Start generating products \n");
        long startTime = System.currentTimeMillis();

        long numberOfMessages = Stream.generate(() -> new ProductDTO
                        (generateName(), generateCategoryId()))
                .filter(validator::isValid)
                .limit(NUMBER_OF_PRODUCTS)
                .peek(products::add)
                .count();

        logRps(startTime, numberOfMessages);

        return products;
    }

    private String generateName() {
        StringBuilder sb = new StringBuilder();
        int length = random.nextInt(NAME_MAX_LENGTH - NAME_MIN_LENGTH + 1) + NAME_MIN_LENGTH;
        for (int i = 0; i < length; i++) {
            String allowedCharacters = "abcdefghijklmnopqrstuvwxyz";
            int indexOfLetter = random.nextInt(allowedCharacters.length());
            sb.append(allowedCharacters.charAt(indexOfLetter));
        }

        return sb.toString();
    }

    private int generateCategoryId() {
        return random.nextInt(NUMBER_OF_TYPES + 1);
    }

    private void logRps(long startTime, long numberOfProducts) {
        double tookTime = (System.currentTimeMillis() - startTime) / 1000.0;
        double rps = numberOfProducts / tookTime;
        log.info("Generated {} products for {} second", numberOfProducts, tookTime);
        log.info("rps: {}\n", rps);
    }
}
