package com.stepanov.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * Class loads data from property file
 */
public class PropertiesUtil {
    private static final Logger LOG = LoggerFactory.getLogger(PropertiesUtil.class);
    public static final Properties PROPERTIES = new Properties();

    static {
        loadPropFile();
    }

    private PropertiesUtil() {
    }

    public static String get(String key) {
        return PROPERTIES.getProperty(key);
    }

    private static void loadPropFile() {
        String fileName = "config.properties";
        try (InputStream stream = PropertiesUtil.class.getClassLoader().getResourceAsStream(fileName)) {
            InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
            LOG.trace("Reading property file");
            PROPERTIES.load(reader);
        } catch (IOException e) {
            LOG.error("Property file not found", e);
        }
    }

}
