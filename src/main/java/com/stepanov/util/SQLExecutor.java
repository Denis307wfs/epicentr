package com.stepanov.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLExecutor {
    private final Logger log = LoggerFactory.getLogger(SQLExecutor.class);
    private final ConnectionManager connectionManager;

    public SQLExecutor(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public void executeFile(String fileName) {
        log.debug("Execute {} file", fileName);
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            StringBuilder sqlScript = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                // Pass comments and empty lines
                if (!line.trim().startsWith("--") && !line.trim().isEmpty()) {
                    sqlScript.append(line);
                    // execute when end line
                    if (line.trim().endsWith(";")) {
                        executeSqlStatement(sqlScript.toString());
                        sqlScript.setLength(0);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            log.error("File {} not found", fileName, e);
        } catch (IOException e) {
            log.error("File {} was not read", fileName, e);
        }
    }

    private void executeSqlStatement(String sqlScript) {
        try (Connection connection = connectionManager.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(sqlScript);
        } catch (SQLException e) {
            log.error("Can not read sql script {}", sqlScript, e);
        }
    }


}
