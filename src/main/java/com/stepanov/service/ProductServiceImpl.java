package com.stepanov.service;

import com.stepanov.dao.DAOFactory;
import com.stepanov.dao.ProductDAO;
import com.stepanov.dto.ProductDTO;

import java.util.List;

public class ProductServiceImpl implements ProductService {
    private final ProductDAO productDAO = DAOFactory.getProductDAO();

    @Override
    public void insertProducts(List<ProductDTO> products) {
        productDAO.insert(products);
    }
}
