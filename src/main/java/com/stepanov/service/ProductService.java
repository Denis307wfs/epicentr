package com.stepanov.service;

import com.stepanov.dto.ProductDTO;

import java.util.List;

public interface ProductService {
    void insertProducts(List<ProductDTO> products);
}
