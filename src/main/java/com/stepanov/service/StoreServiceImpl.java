package com.stepanov.service;

import com.stepanov.dao.DAOFactory;
import com.stepanov.dao.StoreDAO;

public class StoreServiceImpl implements StoreService {
    private final StoreDAO storeDAO = DAOFactory.getStoreDAO();

    @Override
    public void insertStoreProducts() {
        storeDAO.insert();
    }

    @Override
    public String getShopAddressWithMaxCountOfProductsByType(String type) {
        return storeDAO.findMaxCountOfProductsInShopByType(type);
    }
}
