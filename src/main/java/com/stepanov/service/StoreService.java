package com.stepanov.service;

public interface StoreService {

    void insertStoreProducts();
    String getShopAddressWithMaxCountOfProductsByType(String type);
}
